// This is used to get the contents of the express package to be used by the application
const express = require("express");

// Creates an application using express
// This creates an express application and stores it in a variable
// In layman's term, app is our server
const app = express();

// for our application server to run, we need a port to listen to
const port = 3000;

// Set up for allowing the server to handle data from requests
// Allows our app to read JSON data
// Middleware is a request handler that has access to the application's request and response cycle
app.use(express.json())

let users = [
	{
		username: "TStark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	}
];

// [SECTION] Routes

// Express has methods corresponding to each http method

// This routes expects to receive a GET request at the base URI
// This will return a simple message back to the client
	app.get("/", (req, res) => {

		// Combines writeHead() and end()
		// It used the express JS modules' method instead to send a response back to the client
		res.send("Hello World")
	});


	app.get("/greeting", (req, res) => {
		res.send("Hello from Batch244-Mondano")
	});

// This route expects to receive a POST request

app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
});

// This route expects to receive a post request at the URI "/signup"

app.post("/signup", (req, res) => {
	if(req.body.username !== "" && req.body.password !== "" && req.body.email !== ""){
		users.push(req.body)
		res.send(`User ${req.body.username} is successfully registered!`)
	}
	else{
		res.send("Please input BOTH username and password")
	}
});

// Retrieving all users
app.get("/users", (req, res) => {
	res.send(users);
})

// This route expects to receive a PUT request
// This will update the password of a user that matchers the information provided in the client/Postman
app.put("/change-password", (req, res) => {
	let message;

	for (let i = 0; i < users.length; i++){
		if (req.body.username == users[i].username){
			users[i].password = req.body.password

			message = `User ${req.body.username}'s password has been updated`
			break;
		} else {
			message = "User does not exist."
		}
	}
	res.send(message);
})


// 1. Create a GET route that will access the "/home" route that will print out a simple message.
app.get("/home", (req, res) => {
		res.send("Welcome to home page.")
	});
// 2. Process a GET request at the "/home" route using postman.
// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
// 4. Process a GET request at the "/users" route using postman.
// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete("/delete-user", (req, res) => {
    let message;

    for(let i = 0; i < users.length; i++){

        if (req.body.username == users[i].username){    
            users.splice(i, 1)
            message = `User ${req.body.username} has been deleted.`
            break;

        } else {
            message = "User does not exist."
        }
    }
    res.send(message);
});

// 6. Process a DELETE request at the "/delete-user" route using postman.

app.listen(port, () => console.log(`Server is running at port ${port}`))